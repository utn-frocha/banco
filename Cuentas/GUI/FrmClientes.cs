﻿using CapaEntidad;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class FrmClientes : Form
    {
        public FrmClientes()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            LCliente newCliente = new LCliente();
            newCliente.insertarCliente(txtCedula.Text,txtNombre.Text,int.Parse(txtEdad.Text),int.Parse(txtCodCliente.Text),int.Parse(txtCodAgencia.Text),txtNombreAgencia.Text,
                double.Parse(txtLimiteSaldo.Text),txtNumeroTarjeta.Text,double.Parse(txtSaldoTarjeta.Text),txtTipoTarjeta.Text,txtCedulaAsesor.Text,txtNombreAsesor.Text,int.Parse(txtEdadAsesor.Text));
            lstCliente.Items.Add(newCliente.cargarCliente());


        }

        private void FrmClientes_Load(object sender, EventArgs e)
        {

        }
    }
}
