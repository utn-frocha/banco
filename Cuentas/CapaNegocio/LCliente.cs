﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;

namespace CapaNegocio
{
    public class LCliente
    {
        private Cliente Persona { get; set; }
        LinkedList<Cliente> cliente = new LinkedList<Cliente>();
        public LCliente()
        {
            Persona = new Cliente();
        }

        public void insertarCliente(string cedula, string nombre, int edad, int codigoCliente, 
            int codAgencia, string nomAgencia, 
            double limiteSaldo, string numeroTarjeta, double saldoTarjeta, string tipoTarjeta, 
            string cedulaAsesor, string nombreAsesor, int edadAsesor)

        {
            Persona.Cedula = cedula;
            Persona.Nombre = nombre;
            Persona.Edad = edad;
            Persona.CodigoCliente = codigoCliente;

            /*Cargar datos por composición*/

            Persona.OTarjeta.LimiteSaldo = limiteSaldo;
            Persona.OTarjeta.NumeroTerjeta = numeroTarjeta;
            Persona.OTarjeta.Saldo = saldoTarjeta;
            Persona.OTarjeta.Tipo = tipoTarjeta;

            /*agregar la agencia por metodo de agregación*/

            Agencia sucu = new Agencia();
            sucu.Codigo = codAgencia;
            sucu.Nombre = nomAgencia;
            sucu.asesor.Cedula = cedulaAsesor;
            sucu.asesor.Nombre = nombreAsesor;
            sucu.asesor.Edad = edadAsesor;
            Persona.OAgencia = sucu;
            cliente.AddLast(Persona);
 
        }

        public string cargarCliente() 
        {
            string clientes ="";
            foreach (var c in cliente)
            {
                clientes += c.ToString() + Environment.NewLine; 
            }
            return clientes;
        }
    }
}
