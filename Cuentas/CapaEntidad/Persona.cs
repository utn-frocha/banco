﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public abstract class Persona 
    {
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public int Edad { get; set; }

        public Persona()
        {
        }

        public Persona(string cedula, string nombre, int edad)
        {
            Cedula = cedula;
            Nombre = nombre;
            Edad = edad;
        }

        public override string ToString()
        {
            return Nombre + "" + Cedula + "" + Edad + "";
        }
    }
}
