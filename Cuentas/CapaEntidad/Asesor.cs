﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Asesor : Persona, IProcesos
    {
        public Asesor():base()
        {
        }

        public Asesor(string cedula, string nombre, int edad) : base(cedula, nombre, edad)
        {

        }

        public string Invertir()
        {
            return "Invierte el 50% del salario en acciones";
        }

        public string Renunciar()
        {
            return "Renuncia si las calidades del puesto cambian";
        }
    }
}
