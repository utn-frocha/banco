﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Agencia
    {
        public int Codigo { get; set; }
        public string Nombre { get; set; }
        public Asesor asesor { get; set; }

        public Agencia(int codigo, string nombre)
        {
            Codigo = codigo;
            Nombre = nombre;
            asesor = new Asesor(); 
        }

        public Agencia()
        {
            asesor = new Asesor();
        }

        public override string ToString()
        {
            return Codigo+" "+Nombre;
        }
    }
}
