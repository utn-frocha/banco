﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Tarjeta
    {
        public string NumeroTerjeta { get; set; }
        public double Saldo { get; set; }
        public double LimiteSaldo { get; set; }
        public string Tipo { get; set; }


        public Tarjeta()
        {

        }

        public Tarjeta(string numeroTerjeta, double saldo, double limiteSaldo, string tipo)
        {
            NumeroTerjeta = numeroTerjeta;
            Saldo = saldo;
            LimiteSaldo = limiteSaldo;
            Tipo = tipo;
        }

        public override string ToString()
        {
            return NumeroTerjeta+ " " + Saldo+ " " + LimiteSaldo+ " " + Tipo;
        }
    }

    
}
