﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class Cliente : Persona, IProcesos
    {
        public int CodigoCliente { get; set; }
        public Agencia OAgencia { get; set; }
        public Tarjeta OTarjeta { get; set; }

        /* Vincular una clase por composición */
        public Cliente() : base ()
        {
            OTarjeta = new Tarjeta();

        }

        public Cliente(string cedula, string nombre, int edad, int codigoCliente, Agencia agencia, Tarjeta tarjeta)
            : base (cedula, nombre, edad)
        {
            Cedula = cedula;
            Nombre = nombre;
            Edad = edad;
            CodigoCliente = codigoCliente;
            OAgencia = agencia;
            OTarjeta = tarjeta;
            OTarjeta = new Tarjeta();
        }

        public override string ToString()
        {
            return Cedula + " " + Nombre + " " + OTarjeta.NumeroTerjeta + " " + OTarjeta.Saldo
                + " " + OTarjeta.LimiteSaldo + " " + OTarjeta.Tipo;
        }

        public string Renunciar()
        {
            return "Cliente no va a renunciar";
        }

        public string Invertir()
        {
            return "Invierte cada cuatro años";
        }
    }
}
